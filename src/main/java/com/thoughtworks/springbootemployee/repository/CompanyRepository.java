package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyRepository {
    private final List<Company> companyList = new ArrayList<>();
    private EmployeeRepository employeeRepository = new EmployeeRepository();

    public List<Company> getAllCompany() {
        return companyList;
    }

    public Company findCompanyById(Long id) {
        return companyList.stream().filter(companyItem -> companyItem.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Company> getCompanyByPaging(Integer page, Integer size) {
        return companyList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    public void addCompany(Company company) {
        company.setId(generateId());
        companyList.add(company);
    }

    private Long generateId() {
        long maxId = companyList.stream().mapToLong(Company::getId).max().orElse(0L);
        return maxId + 1;
    }

    public List<Employee> findEmployeeGroupByCompany(Long id) {
        List<Employee> employeeList = employeeRepository.getAllEmployee();
        return employeeList.stream().filter(employee ->
                employee.getCompanyId().equals(id)).collect(Collectors.toList());
    }

    public void updateCompanyInfo(Company company) {
        Company willUpdateCompany = companyList.stream().filter(companyItem ->
                companyItem.getId().equals(company.getId())).findFirst().orElse(null);
        if (willUpdateCompany != null) {
            willUpdateCompany.setName(company.getName());
        }
    }

    public void deleteCompany(Long id) {
        Company companyWillBeDeleted = companyList.stream().filter(company ->
                company.getId().equals(id)).findFirst().orElse(null);
        List<Employee> employeeList = employeeRepository.getAllEmployee().stream().filter(employee ->
                employee.getCompanyId().equals(id)).collect(Collectors.toList());
        if (employeeList != null) {
            employeeList.forEach(employee -> employeeRepository.deleteEmployeeById(employee.getId()));
        }
        companyList.remove(companyWillBeDeleted);
    }
}
