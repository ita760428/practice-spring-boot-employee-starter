package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeRepository {
private final static List<Employee> employeeList = new ArrayList<>();

    public EmployeeRepository() {
    }

    public List<Employee> getAllEmployee() {
        return employeeList;
    }

    public Employee getEmployeeById(int id) {
        return employeeList.stream().filter(employee -> employee.getId() == id).collect(Collectors.toList()).get(0);
    }
    public List<Employee> getEmployeesByGender(String gender) {
        return employeeList.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());

    }

    public void addEmployee(Employee employee) {
        employee.setId(generateId());
        employeeList.add(employee);

    }

    private Long generateId() {
        long maxId = employeeList.stream().mapToLong(Employee::getId).max().orElse(0L);
        return maxId + 1;
    }

    public void updateEmployeeInfo(Employee employee) {
        Employee willUpdateEmployee = employeeList.stream().filter(employeeItem -> employeeItem.getId().equals(employee.getId())).findFirst().orElse(null);
        if (willUpdateEmployee != null) {
            willUpdateEmployee.setAge(employee.getAge());
            willUpdateEmployee.setSalary(employee.getSalary());
        }
    }

    public void deleteEmployeeById(Long id) {
        Employee willDeleteEmployee = employeeList.stream().filter(employee -> employee.getId().equals(id)).findFirst().orElse(null);
        if (willDeleteEmployee != null) {
            employeeList.remove(willDeleteEmployee);
        }
    }

    public List<Employee> getEmployeeByPaging(Integer page, Integer size) {
        return employeeList.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }
}
