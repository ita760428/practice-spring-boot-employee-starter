package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    EmployeeRepository employeeRepository = new EmployeeRepository();


    @GetMapping
    public List<Employee> getEmployeeList() {
        return employeeRepository.getAllEmployee();
    }

    @GetMapping(value = "/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeRepository.getEmployeeById(id);

    }

    @GetMapping(params = "gender")
    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return employeeRepository.getEmployeesByGender(gender);

    }

    @PostMapping
    public void addEmployee(@RequestBody Employee employee) {
        employeeRepository.addEmployee(employee);

    }

    @PutMapping
    public void updateEmployeeInfo(@RequestBody Employee employee) {
        employeeRepository.updateEmployeeInfo(employee);
    }

    @DeleteMapping("/{id}")
    public void deleteEmployeeById(@PathVariable Long id) {
       employeeRepository.deleteEmployeeById(id);

    }
    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeeByPaging(@RequestParam Integer page,@RequestParam Integer size){
       return employeeRepository.getEmployeeByPaging(page,size);
    }

}
