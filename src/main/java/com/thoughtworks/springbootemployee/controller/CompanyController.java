package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    CompanyRepository companyRepository = new CompanyRepository();
    EmployeeRepository employeeRepository = new EmployeeRepository();

    @GetMapping
    public List<Company> getAllCompany(){
        return companyRepository.getAllCompany();
    }

    @GetMapping("/{id}")
    public Company findCompanyById(@PathVariable Long id){
        return companyRepository.findCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> findEmployeeGroupByCompany(@PathVariable Long id){
        return companyRepository.findEmployeeGroupByCompany(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompanyByPaging(@RequestParam Integer page, @RequestParam Integer size){
        return companyRepository.getCompanyByPaging(page,size);
    }

    @PostMapping
    public void addCompany(@RequestBody Company company){
        companyRepository.addCompany(company);
    }

    @PutMapping
    public void updateCompany(@RequestBody Company company){
        companyRepository.updateCompanyInfo(company);
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable Long id){
        companyRepository.deleteCompany(id);
    }
}
